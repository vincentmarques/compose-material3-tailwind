# Material3 for Compose HTML

Blackbox implementation of the [Material3](https://m3.material.io/) specification for [Compose HTML](https://github.com/JetBrains/compose-multiplatform#compose-html) using [TailwindCSS](https://tailwindcss.com/).

## License

This project is licensed under the [Apache 2.0 license](LICENSE).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
- To learn more about our coding conventions and workflow, see the [OpenSavvy Wiki](https://gitlab.com/opensavvy/wiki/-/blob/main/README.md#wiki).
- This project is based on the [OpenSavvy Playground](docs/playground/README.md), a collection of preconfigured project templates.
