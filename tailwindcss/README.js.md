# Module TailwindCSS for Kotlin

CSS utilities to declare color themes, TailwindCSS configuration, etc.

Depend on this module directly if you want to use the Material3 colors without our implementation of the Material3 components.

To configure the color scheme, see [InstallColorScheme][opensavvy.material3.css.InstallColorScheme].
