package opensavvy.material3.tailwind.actions.fab

enum class FloatingActionButtonTheme {
	PrimaryColor,
	SecondaryColor,
	TertiaryColor,
	SurfaceColor,
}
